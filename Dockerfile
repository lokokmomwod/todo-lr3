FROM python:3.10


COPY ./requirements.txt /code/requirements.txt

ARG PROXY
#WORKDIR /code

RUN pip install --no-cache-dir --upgrade uvicorn;
RUN pip install --no-cache-dir --upgrade python-multipart;
RUN if [ -z "$PROXY" ]; then \
        pip install --no-cache-dir --upgrade -r /code/requirements.txt; \
    else \
        pip install --proxy "$PROXY" --no-cache-dir --upgrade -r /code/requirements.txt; \
    fi


COPY ./app /code/app

WORKDIR /code/app

CMD ["uvicorn", "app:app", "--reload", "--host", "0.0.0.0", "--port", "8000"]
