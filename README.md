### Основной запуск приложения без применения Docker-compose

```bash
uvicorn app:app --reload
```

### Создание Docker-контейнера

```bash
sudo docker build -t 2020-3-08-lr3 .
```

### With database save

```bash
sudo docker run --rm -p 80:80 -v "${PWD}/data/":/code/data 2020-3-08-lr3
```

### Development

```bash
sudo docker run --rm -v ${pwd}/app:/code/app -p 80:80 2020-3-08-lr3
```

### Запуск Docker через proxy

```bash
sudo docker --build-arg PROXY=http://login:pass@192.168.232.1:3128 -t 2020-3-08-lr3 .
sudo docker run --rm -v ${pwd}/app:/code/app -p 80:80 2020-3-08-lr3
```
