import json
from random import shuffle

class Authorization:
    @staticmethod
    def custom_hash(password: str):
        return 'hash' + 'hash'.join(list(password)) + 'hash'

    @staticmethod
    def generate_token(login: str):
        return 'tkn' + ''.join(login)

    @staticmethod
    def check_user(login: str, password: str):
        hashed_password = Authorization.custom_hash(password)
        try:
            with open('../databases/users.json', 'r') as todo_file:
                data = json.load(todo_file)
                for i in range(1, len(data)):
                    if data["user #"+str(i)]["username"] == login and data["user #"+str(i)]["hashed_password"] == hashed_password:
                        token = Authorization.generate_token(login)
                        print(f"[TOKEN]: {token}")
                        break
                return token
        except Exception as err:
            print(err)
            return ""

    @staticmethod
    def add_user(login: str, password: str):
        hashed_password = Authorization.custom_hash(password)
        try:
            with open('../databases/users.json', 'r') as todo_file:
                data = json.load(todo_file)
                for i in range(1, len(data)+1):
                    if data["user #"+str(i)]["username"] == login:
                        token = 0
                        print(f"User {login} already exist")
                        return token
            with open('../databases/users.json', 'w') as todo_file:
                data['user #' + f'{len(data)+1}'] = {
                        'username': f'{login}',
                        'password': f'{hashed_password}'
                    }
                json.dump(data, todo_file)
            token = Authorization.generate_token(login)
            return token
        except Exception as err:
            print(err)
            return ""

