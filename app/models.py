"""Todo models
"""
from ctypes import Union
from enum import Enum

from sqlalchemy.ext.mutable import MutableList
from sqlalchemy import PickleType
from pydantic import BaseModel
from sqlalchemy import Column, Integer, Boolean, Text
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Todo(Base):
    """Todo model
    """
    __tablename__ = 'todos'
    id = Column(Integer, primary_key=True)
    title = Column(Text)
    completed = Column(Boolean, default=False)
    details = Column(Text)
    tag = Column(Text, default="")
    creation_date = Column(Text)
    completion_date = Column(Text)
    full = Column(Text, default="Общее")
    image = Column(Text, nullable=True)
    message = Column(MutableList.as_mutable(PickleType), default=[])
    author = Column(Text, default="Неизвестно")


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    username = Column(Text, default="")
    hashed_password = Column(Text, default="")


class Tags(str, Enum):
    studies = "studies"
    personal = "personal"
    plans = "plans"


class Fullname(str, Enum):
    """Fullname model
    """
    fullname1 = "2020-3-04-kva"
    fullname2 = "2020-3-15-mur"
    fullname3 = "2020-3-07-kos"
    fullname4 = "2020-3-17-nik"
    fullname5 = "Общее"

    def __repr__(self):
        return f'<Todo {self.id}>'
