"""Database for todo
"""
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import Session  # pylint: disable=unused-import
import os

from models import Base

DB_URL = os.path.join("postgresql://user:user@postgres:5432/todos")
ENGINE = create_engine(DB_URL)
# for logging all SQL-queries
SESSIONLOCAL = sessionmaker(autocommit=False, autoflush=False, bind=ENGINE)


def init_db():
    """Init database, create all models as tables
    """
    # check_same_thread is for SQLite only
    Base.metadata.create_all(bind=ENGINE)


def get_db():
    """Create session/connection for each request
    """
    database = SESSIONLOCAL()
    try:
        yield database
    finally:
        database.close()


